<?php

use Illuminate\Support\Facades\Route;

$this->settingRole = config("settings.role");

// Route::group([
//     "namespace" => "Home",
//     "as" => "home.",
// ], function () {
//     Route::group([
//         "namespace" => "Dashboards"
//     ], function () {
//         Route::resource("dashboards", "DashboardController")->only(["index"]);
//         Route::group([
//             "as" => "dashboards.",
//         ], function () {
//             Route::get("index", "DashboardController@index")->name("index");
//             Route::get("", "DashboardController@index")->name("index");
//         });
//     });
// });

Route::namespace("Auth")->group(function () {
    Route::get("", "LoginController@showLoginForm")->name("home.dashboards.index");
    Route::get("login", 'LoginController@showLoginForm')->name("login");
    Route::post("login", 'LoginController@login')->name("login");
    Route::post("logout", 'LoginController@logout')->name("logout");
});

Route::middleware(["auth"])->group(function () {
    //Administrator
    Route::group([
        "middleware" => ["role:" . implode("|", $this->settingRole["administrator"])],
        "namespace" => "Administrator",
        "as" => "administrator.",
        "prefix" => "administrator"
    ], function () {
        Route::group([
            "namespace" => "Dashboards",
        ], function () {
            Route::resource("dashboards", "DashboardController")->only(["index"]);
            Route::group([
                "as" => "dashboards.",
            ], function () {
                Route::get("", "DashboardController@index")->name("index");
            });
        });

        Route::group([
            "namespace" => "Profiles",
            "as" => "profile.",
            "prefix" => "profile"
        ], function () {
            Route::get("index", "ProfileController@index")->name("index");
            Route::get("", "ProfileController@index")->name("index");
            Route::put("", "ProfileController@update")->name("update");
            Route::delete("", "ProfileController@destroy")->name("destroy");
            Route::put("password", "ProfileController@updatePassword")->name("update-password");
            Route::put("avatar", "ProfileController@updateAvatar")->name("update-avatar");
        });

        Route::group([
            "middleware" => ["role:" . $this->settingRole["administrator"]["superadmin"]],
        ], function () {
            Route::group([
                "namespace" => "Roles"
            ], function () {
                Route::resource("roles", "RoleController")->except(["show"]);
            });

            Route::group([
                "namespace" => "Permissions"
            ], function () {
                Route::resource("permissions", "PermissionController")->except(["show", "create", "edit"]);
            });
        });

        Route::group([
            "namespace" => "Users"
        ], function () {
            Route::resource("users", "UserController")->except(["show"]);
            Route::group([
                "as" => "users.",
                "prefix" => "users"
            ], function () {
                Route::put("{id}/password", "UserController@updatePassword")->name("update-password");
                Route::put("{id}/status", "UserController@updateStatus")->name("update-status");
                Route::put("{id}/avatar", "UserController@updateAvatar")->name("update-avatar");

                Route::group([
                    "as" => "deleted.",
                    "prefix" => "deleted"
                ], function () {
                    Route::get("index", "UserDeletedController@index")->name("index");
                    Route::get("", "UserDeletedController@index")->name("index");
                    Route::post("{id}/restore", "UserDeletedController@restore")->name("restore");
                });
            });
        });

        Route::group([
            "namespace" => "Variables"
        ], function () {
            Route::resource("variables", "VariableController")->except(["show"]);
        });

        Route::group([
            "namespace" => "Rules"
        ], function () {
            Route::resource("rules", "RuleController")->except(["show"]);
        });

        Route::group([
            "namespace" => "Executes"
        ], function () {
            Route::resource("executes", "ExecuteController")->except(["edit", "update"]);
        });
    });
});
