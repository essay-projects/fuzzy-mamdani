<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        "full_name",
        "avatar"
    ];

    public function user()
    {
        return $this->hasOne(\App\User::class);
    }
}
