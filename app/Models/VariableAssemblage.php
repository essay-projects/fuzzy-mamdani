<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariableAssemblage extends Model
{
    protected $fillable = [
        "variable_id",
        "name",
        "left_point_value",
        "left_midpoint_value",
        "right_midpoint_value",
        "right_point_value"
    ];

    public function variable()
    {
        return $this->belongsTo(Variable::class);
    }
}
