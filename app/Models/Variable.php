<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $fillable = [
        "name",
        "type",
        "column_name"
    ];

    public function assemblages()
    {
        return $this->hasMany(VariableAssemblage::class);
    }
}
