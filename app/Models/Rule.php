<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable = [
        "variable_assemblage_id"
    ];

    public function conditions()
    {
        return $this->hasMany(RuleCondition::class);
    }

    public function assemblage()
    {
        return $this->belongsTo(VariableAssemblage::class, "variable_assemblage_id", "id");
    }
}
