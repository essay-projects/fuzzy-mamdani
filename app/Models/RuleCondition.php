<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RuleCondition extends Model
{
    protected $fillable = [
        "rule_id",
        "variable_id",
        "variable_assemblage_id"
    ];

    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    public function variable()
    {
        return $this->belongsTo(Variable::class, "variable_id", "id");
    }

    public function assemblage()
    {
        return $this->belongsTo(VariableAssemblage::class, "variable_assemblage_id", "id");
    }
}
