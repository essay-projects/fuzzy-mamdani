<?php

namespace App\Http\Controllers\Administrator\Variables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Variable;
use App\Models\VariableAssemblage;

class VariableController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $variables = Variable::all();
            return datatables($variables)
                ->addColumn("column_assemblages", function ($data) {
                    if ($data->assemblages->count()) {
                        $assemblages = '<small>';
                        foreach ($data->assemblages as $key => $assemblage) {
                            if ($key !== 0) {
                                $assemblages .= "<br>";
                            }

                            $assemblages .= $assemblage->name . " (" . $assemblage->left_point_value . " - " . $assemblage->right_point_value . ")";
                        }
                        $assemblages .= '</small>';
                    } else {
                        $assemblages = '<small class="font-italic m-1">Assemblage Not Found</small>';
                    }

                    return $assemblages;
                })
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <a href="' . route('administrator.variables.edit', $data->id) . '"  class="btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </a>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Destroy
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.variables.index"
        );
    }

    public function create()
    {
        return view(
            "administrator.variables.create"
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "type" => "required|in:INPUT,OUTPUT",
            "column_name" => "nullable|string",
            "assemblages"    => "required|array|min:1",
            "assemblages.*.name"  => "required|string",
            "assemblages.*.left_point_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.left_midpoint_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.right_midpoint_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.right_point_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
        ]);

        $variable = Variable::updateOrCreate(
            [
                "name" => $request->name
            ],
            [
                "type" => $request->type,
                "column_name" => $request->column_name ?? NULL
            ]
        );

        foreach ($request->assemblages as $assemblage) {
            VariableAssemblage::updateOrCreate(
                [
                    "variable_id" => $variable->id,
                    "name" => $assemblage["name"]
                ],
                [
                    "left_point_value" => $assemblage["left_point_value"],
                    "left_midpoint_value" => $assemblage["left_midpoint_value"],
                    "right_midpoint_value" => $assemblage["right_midpoint_value"],
                    "right_point_value" => $assemblage["right_point_value"]
                ]
            );
        }

        return redirect()->route(
            "administrator.variables.index"
        )->with("success", "Variable created successfully.");
    }

    public function edit($id)
    {
        $variable = Variable::findOrFail($id);

        return view(
            "administrator.variables.edit",
            compact("variable")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "type" => "required|in:INPUT,OUTPUT",
            "column_name" => "nullable|string",
            "assemblages"    => "required|array|min:1",
            "assemblages.*.name"  => "required|string",
            "assemblages.*.left_point_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.left_midpoint_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.right_midpoint_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
            "assemblages.*.right_point_value"  => "required|numeric|regex:/^\d+(\.\d{1,})?$/",
        ]);

        $variable = Variable::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name,
                "type" => $request->type,
                "column_name" => $request->column_name ?? NULL
            ]
        );

        $asesemblagesTemporaries = $variable->assemblages->pluck("id", "id")->toArray();
        foreach ($request->assemblages as $assemblageId => $assemblage) {
            if (is_numeric($assemblageId) && is_string($assemblage["name"])) {
                if (in_array($assemblageId, $asesemblagesTemporaries)) {
                    $assemblageTemporary = VariableAssemblage::updateOrCreate(
                        [
                            "id" => $assemblageId
                        ],
                        [
                            "variable_id" => $variable->id,
                            "name" => $assemblage["name"],
                            "left_point_value" => $assemblage["left_point_value"],
                            "left_midpoint_value" => $assemblage["left_midpoint_value"],
                            "right_midpoint_value" => $assemblage["right_midpoint_value"],
                            "right_point_value" => $assemblage["right_point_value"]
                        ]
                    );

                    unset($asesemblagesTemporaries[$assemblageTemporary->id]);
                } else {
                    VariableAssemblage::updateOrCreate(
                        [
                            "variable_id" => $variable->id,
                            "name" => $assemblage["name"]
                        ],
                        [
                            "left_point_value" => $assemblage["left_point_value"],
                            "left_midpoint_value" => $assemblage["left_midpoint_value"],
                            "right_midpoint_value" => $assemblage["right_midpoint_value"],
                            "right_point_value" => $assemblage["right_point_value"]
                        ]
                    );
                }
            }
        }

        if ($asesemblagesTemporaries) {
            $variable->assemblages()->whereIn("id", $asesemblagesTemporaries)->delete();
        }

        return redirect()->route(
            "administrator.variables.edit",
            $variable->id
        )->with("success", "Variable updated successfully.");
    }

    public function destroy($id)
    {
        $variable = Variable::findOrFail($id);
        $variable->delete();

        return redirect()->route(
            "administrator.variables.index"
        )->with("success", "Variable deleted successfully.");
    }
}
