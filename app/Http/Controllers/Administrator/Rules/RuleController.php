<?php

namespace App\Http\Controllers\Administrator\Rules;

use App\Http\Controllers\Controller;
use App\Models\Rule;
use App\Models\RuleCondition;
use Illuminate\Http\Request;

use App\Models\Variable;
use App\Models\VariableAssemblage;

class RuleController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $rules = Rule::all();
            return datatables($rules)
                ->addColumn("column_rule", function ($data) {
                    if ($data->conditions->count()) {
                        $rule = 'IF ';
                        foreach ($data->conditions as $key => $condition) {
                            if ($key !== 0) {
                                $rule .= " AND ";
                            }

                            $rule .= '<span style="font-weight: normal;">';
                            $rule .= $condition->assemblage->variable->name . " " . $condition->assemblage->name;
                            $rule .= '</span>';
                        }

                        $rule .= ' THEN <span style="font-weight: normal;">' . $data->assemblage->variable->name . " " . $data->assemblage->name . '</span>';
                    } else {
                        $rule = '<small class="font-italic m-1">Rule Not Found</small>';
                    }

                    return $rule;
                })
                ->addColumn("column_actions", function ($data) {
                    return '
                    <div class="row m-0">
                        <div class="col mr-1 ml-1 p-0">
                            <a href="' . route('administrator.rules.edit', $data->id) . '"  class="btn btn-sm btn-info col-sm-12">
                                <i class="fas fa-pencil-alt mr-1"></i>
                                Edit
                            </a>
                        </div>
                        <div class="col mr-1 ml-1 p-0">
                            <button type="button" class="btn btn-sm btn-danger col-sm-12" onclick="handleDestroy(' . $data->id . ')">
                                <i class="fas fa-trash-alt mr-1"></i>
                                Destroy
                            </button>
                        </div>
                    </div>
                    ';
                })
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view(
            "administrator.rules.index"
        );
    }

    public function create()
    {
        $variableAssemblages = VariableAssemblage::whereHas("variable", function ($variable) {
            $variable->whereType("OUTPUT");
        });

        if ($variableAssemblages->count()) {
            $variableAssemblages = $variableAssemblages->get()->keyBy("id")->map(function ($variableAssemblage) {
                return $variableAssemblage->variable->name . " " . $variableAssemblage->name;
            });
            $variables = Variable::whereType("INPUT")->pluck("name", "id")->toArray();

            return view(
                "administrator.rules.create",
                compact("variableAssemblages", "variables")
            );
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "value" => "required|exists:variable_assemblages,id",
            "conditions"    => "required|array|min:1",
            "conditions.*.variable"  => "required|exists:variables,id",
            "conditions.*.assemblage"  => "required|exists:variable_assemblages,id"
        ]);

        $rule = Rule::create(
            [
                "variable_assemblage_id" => $request->value
            ]
        );

        foreach ($request->conditions as $condition) {
            RuleCondition::updateOrCreate(
                [
                    "rule_id" => $rule->id,
                    "variable_id" => $condition["variable"]
                ],
                [
                    "variable_assemblage_id" => $condition["assemblage"]
                ]
            );
        }

        return redirect()->route(
            "administrator.rules.index"
        )->with("success", "Rule created successfully.");
    }

    public function edit($id)
    {
        $variableAssemblages = VariableAssemblage::whereHas("variable", function ($variable) {
            $variable->whereType("OUTPUT");
        });

        if ($variableAssemblages->count()) {
            $rule = Rule::findOrFail($id);

            $variableAssemblages = $variableAssemblages->get()->keyBy("id")->map(function ($variableAssemblage) {
                return $variableAssemblage->variable->name . " " . $variableAssemblage->name;
            });
            $variables = Variable::whereType("INPUT")->pluck("name", "id")->toArray();

            return view(
                "administrator.rules.edit",
                compact("variableAssemblages", "variables", "rule")
            );
        } else {
            abort(404);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "value" => "required|exists:variable_assemblages,id",
            "conditions"    => "required|array|min:1",
            "conditions.*.variable"  => "required|exists:variables,id",
            "conditions.*.assemblage"  => "required|exists:variable_assemblages,id"
        ]);

        $rule = Rule::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "variable_assemblage_id" => $request->value
            ]
        );

        $conditionsTemporaries = $rule->conditions->pluck("id", "id")->toArray();
        foreach ($request->conditions as $condition) {
            if (is_numeric($condition["variable"]) && is_numeric($condition["assemblage"])) {
                $conditionTemporary = RuleCondition::updateOrCreate(
                    [
                        "rule_id" => $rule->id,
                        "variable_id" => $condition["variable"]
                    ],
                    [
                        "variable_assemblage_id" => $condition["assemblage"]
                    ]
                );

                if (in_array($conditionTemporary->id, $conditionsTemporaries)) {
                    unset($conditionsTemporaries[$conditionTemporary->id]);
                }
            }
        }

        if ($conditionsTemporaries) {
            $rule->conditions()->whereIn("id", $conditionsTemporaries)->delete();
        }

        return redirect()->route(
            "administrator.rules.edit",
            $rule->id
        )->with("success", "Rule updated successfully.");
    }

    public function destroy($id)
    {
        $variable = Rule::findOrFail($id);
        $variable->delete();

        return redirect()->route(
            "administrator.rules.index"
        )->with("success", "Rule deleted successfully.");
    }
}
