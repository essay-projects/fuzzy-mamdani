<?php

namespace App\Http\Controllers\Administrator\Executes;

use App\Http\Controllers\Controller;
use App\Models\Rule;
use App\Models\RuleCondition;
use Illuminate\Http\Request;

use App\Models\Variable;

class ExecuteController extends Controller
{
    public function index()
    {
        return redirect()->route(
            "administrator.executes.create"
        );
    }

    public function create()
    {
        $variables = Variable::pluck("name", "id");
        $inputVariables = Variable::whereType("INPUT")->pluck("column_name", "id")->toArray();
        $outputVariables = Variable::whereType("OUTPUT")->pluck("name", "id")->toArray();
        $rules = Rule::all();

        if ($rules->count()) {
            return view(
                "administrator.executes.create",
                compact("variables", "inputVariables", "outputVariables")
            );
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "file" => "required|mimes:csv,txt",
            "multiplier_result" => "required|numeric",
            "decimal_digit" => "required|numeric",
        ]);

        if ($request->file("file")->getClientOriginalExtension() == "csv") {
            $csvFile = $request->file("file")->getPathname();

            $file = fopen($csvFile, "r");
            $data = $successData = $failedData = [];

            $inputVariables = Variable::whereType("INPUT");
            $outputVariables = Variable::whereType("OUTPUT")->first();
            $tempColumns = fgetcsv($file, 0, ",");
            if (is_array($tempColumns) && count($tempColumns) > $inputVariables->count() && $outputVariables) {
                $inputVariableColumnNames = $inputVariables->pluck("column_name")->toArray();
                $columns["variables"] = [];

                // Take the location of the variable column and separate it from other columns
                foreach ($tempColumns as $key => $tempColumn) {
                    $tempColumn = trim($tempColumn);
                    if ($tempColumn) {
                        if (in_array($tempColumn, $inputVariableColumnNames)) {
                            $columns["variables"][$tempColumn] = $key;
                        } else {
                            $columns["descriptions"][$tempColumn] = $key;
                        }
                    }
                }

                $file = fopen($csvFile, "r");
                $i = 0;
                while (!feof($file)) {
                    $textLine = fgetcsv($file, 0, ",");
                    if ($i > 0 && $textLine && count($textLine) > $inputVariables->count()) {
                        $data[$i] = [
                            "description" => "",
                            "variables" => []
                        ];

                        // Concatenate column contents that are not column variables
                        if ($columns["descriptions"]) {
                            $j = 0;
                            foreach ($columns["descriptions"] as $descriptionKey) {
                                $textLine[$descriptionKey] = trim($textLine[$descriptionKey]);
                                if ($textLine[$descriptionKey]) {
                                    $data[$i]["description"] .= $textLine[$descriptionKey];
                                    if ($j != count($columns["descriptions"]) - 1) {
                                        $data[$i]["description"] .= " - ";
                                    }
                                }
                                $j++;
                            }
                        }

                        if ($columns["variables"]) {
                            foreach ($columns["variables"] as $variableColumnName => $variableKey) {
                                $data[$i]["variables"][$variableColumnName] = [
                                    "input_value" => $textLine[$variableKey] ?? 0
                                ];

                                // Look for membership functions
                                $variable = Variable::whereColumnName($variableColumnName)->first();
                                $data[$i]["variables"][$variableColumnName]["assemblages"] = $variable->assemblages()->where([
                                    ["left_point_value", "<=", $textLine[$variableKey]],
                                    ['right_point_value', '>=', $textLine[$variableKey]]
                                ])->get()->keyBy("id")->toArray();

                                $data[$i]["variables"][$variableColumnName]["id"] = $variable->id;
                                $data[$i]["variables"][$variableColumnName]["name"] = $variable->name;
                                $data[$i]["variables"][$variableColumnName]["column_name"] = $variable->column_name;

                                // Calculate the implication value of each set of variables
                                $assemblages = $variable->assemblages()->get();
                                $implications = $data[$i]["variables"][$variableColumnName]["assemblages"];
                                foreach ($assemblages as $assemblage) {
                                    if (array_key_exists($assemblage->id, $data[$i]["variables"][$variableColumnName]["assemblages"])) {
                                        $inputValue =  $data[$i]["variables"][$variableColumnName]["input_value"];
                                        if ($assemblage->left_point_value <= $inputValue && $assemblage->left_midpoint_value >= $inputValue) {
                                            if ($assemblage->left_point_value == $assemblage->left_midpoint_value) {
                                                $implications[$assemblage->id]["value"] = 1;
                                            } else {
                                                $implications[$assemblage->id]["value"] = ($inputValue - $assemblage->left_point_value) / ($assemblage->left_midpoint_value - $assemblage->left_point_value);
                                            }
                                        } else if ($assemblage->left_midpoint_value <= $inputValue && $assemblage->right_midpoint_value >= $inputValue) {
                                            $implications[$assemblage->id]["value"] = 1;
                                        } else if ($assemblage->right_midpoint_value <= $inputValue && $assemblage->right_point_value >= $inputValue) {
                                            if ($assemblage->right_point_value == $assemblage->right_midpoint_value) {
                                                $implications[$assemblage->id]["value"] = 1;
                                            } else {
                                                $implications[$assemblage->id]["value"] = ($assemblage->right_point_value - $inputValue) / ($assemblage->right_point_value - $assemblage->right_midpoint_value);
                                            }
                                        } else {
                                            $implications[$assemblage->id]["value"] = 0;
                                        }
                                    }
                                }

                                $data[$i]["variables"][$variableColumnName]["assemblages"] = $implications;
                            }

                            // Create all possible rules from a variable set
                            $variables = $data[$i]["variables"];
                            $j = 0;
                            $assemblageIds = [];
                            foreach ($variables as $variableKey => $variable) {
                                $assemblageIdsTemporaries = [];
                                foreach ($variable["assemblages"] as $assemblage) {
                                    if ($j === 0) {
                                        $assemblageIds[] = [$assemblage["id"]];
                                    } else {
                                        foreach ($assemblageIds as $key => $assemblageId) {
                                            array_push($assemblageId, $assemblage["id"]);
                                            $assemblageIdsTemporaries[] = $assemblageId;
                                        }
                                    }
                                }

                                if ($j !== 0) {
                                    $assemblageIds = $assemblageIdsTemporaries;
                                }
                                $j++;
                            }

                            $rules = Rule::with(["assemblage.variable", "conditions.assemblage.variable"])->get();
                            $is_founded = false;
                            $multiplicationResult = $increaseInYield = 0;
                            foreach ($rules as $rule) {
                                // Look for rules that are already in the database
                                foreach ($assemblageIds as $assemblageId) {
                                    $ruleConditionsCountTemp = RuleCondition::whereRuleId($rule->id)->whereIn("variable_assemblage_id", $assemblageId)->count();
                                    if ($ruleConditionsCountTemp >= count($assemblageId)) {
                                        $ruleTemp = $rule->toArray();
                                        $is_founded = true;
                                    }
                                }

                                // If a rule is found
                                if ($is_founded) {
                                    $conditionValue = [];
                                    // Takes the implication value of each set of variables based on the found rules
                                    foreach ($ruleTemp["conditions"] as $conditionKey => $condition) {
                                        $variableColumnName = $condition["assemblage"]["variable"]["column_name"];
                                        $ruleTemp["conditions"][$conditionKey]["value"] = $data[$i]["variables"][$variableColumnName]["assemblages"][$condition["variable_assemblage_id"]]["value"];
                                        $conditionValue[] = $ruleTemp["conditions"][$conditionKey]["value"];
                                    }

                                    // Find the min value of each variable implication value according to the rule
                                    $ruleTemp["implication"] = [
                                        "value" => min($conditionValue)
                                    ];

                                    // Calculates the defuzzification value of the rule if the implication value is not zero
                                    if ($ruleTemp["implication"]["value"]) {
                                        // Looking for the maximum rule length from the minimum value of the rule obtained
                                        if ($ruleTemp["implication"]["value"] === 1) {
                                            if ($ruleTemp["assemblage"]["left_point_value"] == $ruleTemp["assemblage"]["left_midpoint_value"]) {
                                                $implicationLength = $ruleTemp["assemblage"]["right_midpoint_value"];
                                            } else if ($ruleTemp["assemblage"]["right_point_value"] == $ruleTemp["assemblage"]["right_midpoint_value"]) {
                                                $implicationLength = $ruleTemp["assemblage"]["right_point_value"];
                                            } else {
                                                $implicationLength = $ruleTemp["assemblage"]["right_point_value"];
                                            }
                                        } else {
                                            if ($ruleTemp["assemblage"]["left_point_value"] == $ruleTemp["assemblage"]["left_midpoint_value"]) {
                                                $implicationLength = $ruleTemp["assemblage"]["right_point_value"] - (($ruleTemp["assemblage"]["right_point_value"] - $ruleTemp["assemblage"]["right_midpoint_value"]) * $ruleTemp["implication"]["value"]);
                                            } else if ($ruleTemp["assemblage"]["right_point_value"] == $ruleTemp["assemblage"]["right_midpoint_value"]) {
                                                $implicationLength = $ruleTemp["assemblage"]["left_point_value"] + (($ruleTemp["assemblage"]["left_midpoint_value"] - $ruleTemp["assemblage"]["left_point_value"]) * $ruleTemp["implication"]["value"]);
                                            } else {
                                                $implicationLength = $ruleTemp["assemblage"]["right_midpoint_value"];
                                            }
                                        }
                                        $ruleTemp["implication"]["length"] = $implicationLength;

                                        $ruleTemp["implication"]["multiplication_result"] = $ruleTemp["implication"]["value"] * $ruleTemp["implication"]["length"];
                                        $multiplicationResult += $ruleTemp["implication"]["multiplication_result"];
                                        $increaseInYield += $ruleTemp["implication"]["value"];

                                        $data[$i]["rules"][] = $ruleTemp;
                                    }
                                    $is_founded = false;
                                }
                            }

                            // Set defuzzification value according to form request
                            if (array_key_exists("rules", $data[$i])) {
                                $value = $multiplicationResult / $increaseInYield;
                                $data[$i]["defuzzification"] = [
                                    "top_value" => $multiplicationResult,
                                    "bottom_value" => $increaseInYield,
                                    "value" => number_format($value, $request->decimal_digit, ".", "") * $request->multiplier_result
                                ];
                            } else {
                                $data[$i]["defuzzification"] = NULL;
                            }

                            $successData[] = $data[$i];
                        } else {
                            $failedData[] = $data[$i];
                        }
                    }
                    $i++;
                }
            }
            fclose($file);

            return redirect()->back()
                ->with("success", count($data) . " data executed successfully.")
                ->with("success_data", $successData)
                ->with("failed_data", $failedData)
                ->withInput();
        } else {
            return redirect()->back()->withInput($request->input())->withErrors(["file" => "The file must be a file of type: csv, txt."]);
        }

        return redirect()->route(
            "administrator.executes.index"
        )->with("success", "Execute created successfully.");
    }

    public function show($id)
    {
        // $execute = Execute::findOrFail($id);

        return view(
            "administrator.rules.edit",
            compact("execute")
        );
    }

    public function destroy($id)
    {
        // $variable = Execute::findOrFail($id);
        // $variable->delete();

        return redirect()->route(
            "administrator.executes.index"
        )->with("success", "Execute deleted successfully.");
    }
}
