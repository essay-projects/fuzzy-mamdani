<?php

namespace App\Http\Controllers\Administrator\Dashboards;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\Models\Person;
use App\Models\Rule;
use App\Models\Variable;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUser = auth()->user();
            return $next($request);
        });
    }

    public function index()
    {
        $variables = Variable::all();
        $rules = Rule::all();

        if ($this->authUser->hasRole($this->settingRole["administrator"]["superadmin"])) {
            $permissions = Permission::all();
            $roles = Role::all();
            $people = Person::withTrashed()->orderBy("full_name")->whereHas("user");

            $compact = compact("people", "permissions", "roles");
        } else {
            $people = Person::withTrashed()->orderBy("full_name")->whereHas("user.roles", function ($roles) {
                $roles->where("name", "!=", $this->settingRole["administrator"]["superadmin"]);
            });

            $compact = compact("people", "variables", "rules");
        }

        return view(
            "administrator.dashboards.index",
            $compact
        );
    }
}
