<?php

namespace App\Http\Controllers\Api\Variables;

use App\Http\Controllers\Controller;
use App\Helpers\ResponseHelper;

use App\Models\Variable;

class VariableController extends Controller
{
    public function __construct()
    {
        $this->responHelper = new ResponseHelper;
    }

    public function index()
    {
        $variables = Variable::get();
        if ($variables->count()) {
            return $this->responHelper->successWithData($variables);
        } else {
            return $this->responHelper->errorNotFound();
        }
    }

    public function show($id)
    {
        $variable = Variable::with("assemblages")->find($id);
        if ($variable) {
            return $this->responHelper->successWithData($variable);
        } else {
            return $this->responHelper->errorNotFound();
        }
    }
}
