<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Person;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->responHelper = new ResponseHelper;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|email|max:255",
            "password" => "required|string|min:6",
        ]);

        if (!$validator->fails()) {
            $credentials = $request->only("email", "password");
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return $this->responHelper->errorCredentials();
                }
            } catch (JWTException $e) {
                return $this->responHelper->errorCustom(500, "Could not create token.");
            }

            $user = User::whereEmail($credentials["email"])->first();
            $user->person;
            $user->roles;

            return $this->responHelper->successWithData([
                "token" => $token,
                "user" => $user
            ]);
        } else {
            return $this->responHelper->errorCustom(400, $validator->errors());
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users",
            "password" => "required|string|min:6|confirmed",
            "role" => "required"
        ]);

        if (!$validator->fails()) {
            $user = User::create([
                "email" => $request->get("email"),
                "password" => Hash::make($request->get("password")),
            ]);

            $token = JWTAuth::fromUser($user);

            Person::updateOrCreate(
                [
                    "user_id" => $user->id,
                ],
                [
                    "full_name" => $request->input("name")
                ]
            );
            $user->assignRole([$request->input("role")]);
            $user->person;
            $user->roles;

            return $this->responHelper->successWithData([
                "token" => $token,
                "user" => $user
            ]);
        } else {
            return $this->responHelper->errorCustom(400, $validator->errors());
        }
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return $this->responHelper->errorWithoutCode("User not found.");
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return $this->responHelper->errorCustom(401, "Token expired.");
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->responHelper->errorCustom(401, "Token invalid.");
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $this->responHelper->errorCustom(401, "Authorization Token not found.");
        }

        $user->person;
        $user->roles;
        return $this->responHelper->successWithData($user);
    }
}
