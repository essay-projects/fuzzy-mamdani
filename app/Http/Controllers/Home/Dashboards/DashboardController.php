<?php

namespace App\Http\Controllers\Home\Dashboards;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view(
            "home.dashboards.index"
        );
    }
}
