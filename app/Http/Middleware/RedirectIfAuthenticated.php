<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");
    }

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $authUser = auth()->user();
            if ($authUser->hasRole($this->settingRole["administrator"])) {
                return redirect()->route("administrator.dashboards.index");
            } else {
                auth()->logout();
                return redirect()->route("login");
            }
        }

        return $next($request);
    }
}
