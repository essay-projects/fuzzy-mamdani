<?php

namespace App\Helpers;

class ResponseHelper
{
	public function successWithMetaData($meta, $data)
	{
		return	[
			"meta"	=> $meta,
			"data"	=> $data
		];
	}

	public function successWithData($data)
	{
		return	[
			"meta"	=> [
				"code"		=> 200,
				"message"	=> "Success",
				"error"		=> false
			],
			"data"	=> $data
		];
	}

	public function successWithoutData($message)
	{
		return	[
			"meta"	=> [
				"code"		=> 200,
				"message"	=> $message,
				"error"		=> false
			]
		];
	}

	public function errorVerifyEmail()
	{
		return	[
			"meta"	=> [
				"code"		=> 204,
				"message"	=> "Please Verify Your Email.",
				"error"		=> true
			]
		];
	}

	public function errorAccountSuspended()
	{
		return	[
			"meta"	=> [
				"code"		=> 205,
				"message"	=> "Your Account Has Ben Suspended.",
				"error"		=> true
			]
		];
	}

	public function errorSomething()
	{
		return	[
			"meta"	=> [
				"code"		=> 400,
				"message"	=> "Something Error.",
				"error"		=> true
			]
		];
	}

	public function errorCredentials()
	{
		return	[
			"meta"	=> [
				"code"		=> 206,
				"message"	=> "Invalid Credentials.",
				"error"		=> true
			]
		];
	}

	public function errorWithoutCode($message)
	{
		return	[
			"meta"	=> [
				"code"		=> 400,
				"message"	=> $message,
				"error"		=> false
			]
		];
	}

	public function errorCustom($code, $message)
	{
		return	[
			"meta"	=> [
				"code"		=> $code,
				"message"	=> $message,
				"error"		=> true
			]
		];
	}
}
