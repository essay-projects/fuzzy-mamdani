@extends('administrator.layouts.template')

@section('css')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
@endsection

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Variable</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <form method="POST" action="{{ route('administrator.variables.update', $variable->id) }}">
                        @csrf
                        @method("PUT")

                        <li class="list-group-item p-4">
                            <div class="row">
                                <div class="col-sm-6">
                                    <strong class="text-muted d-block mb-2">Name</strong>
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', $variable->name) }}" required>

                                        @error("name")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <strong class="text-muted d-block mb-2">Type</strong>
                                    <div class="form-group">
                                        {!! Form::select('type', [
                                        "INPUT" => "INPUT",
                                        "OUTPUT" => "OUTPUT",
                                        ], old('type', $variable->type), [
                                        'class' => 'form-control' . ($errors->has('type') ? ' is-invalid' : '' ),
                                        'id' => 'type',
                                        'required' => 'required'
                                        ]) !!}

                                        @error("type")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <strong class="text-muted d-block mb-2">Column Name</strong>
                                    <div class="form-group">
                                        <input type="text" id="column-name" name="column_name" class="form-control @error('column_name') is-invalid @enderror" placeholder="Column Name" value="{{ old('column_name', $variable->column_name) }}" {{ old('type', $variable->type) == 'OUTPUT' ? 'disabled' : '' }}>

                                        @error("column_name")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Assemblages</strong>
                                    <div class="form-group">
                                        @if (old('assemblages') && is_array(old('assemblages')))
                                        @forelse(old('assemblages') as $assemblageKey => $assemblage)
                                        <div class="form-group mb-2" data-type="assemblages">
                                            <div class="input-group mb-2" data-id="{{ $assemblageKey }}">
                                                <input type="text" name="assemblages[{{ $assemblageKey }}][name]" class="form-control @error('assemblages.'.$assemblageKey.'.name') is-invalid @enderror" placeholder="Name" value="{{ $assemblage['name'] }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblageKey }}][left_point_value]" class="form-control text-right @error('assemblages.'.$assemblageKey.'.left_point_value') is-invalid @enderror" placeholder="Left Point Value" value="{{ $assemblage['left_point_value'] }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblageKey }}][left_midpoint_value]" class="form-control text-right @error('assemblages.'.$assemblageKey.'.left_midpoint_value') is-invalid @enderror" placeholder="Left Midpoint Value" value="{{ $assemblage['left_midpoint_value'] }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblageKey }}][right_midpoint_value]" class="form-control text-right @error('assemblages.'.$assemblageKey.'.right_midpoint_value') is-invalid @enderror" placeholder="Right Midpoint Value" value="{{ $assemblage['right_midpoint_value'] }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblageKey }}][right_point_value]" class="form-control text-right @error('assemblages.'.$assemblageKey.'.right_point_value') is-invalid @enderror" placeholder="Right Value" value="{{ $assemblage['right_point_value'] }}" required>

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                        <div class="form-group mb-2" data-type="assemblages">
                                            <div class="input-group mb-2" data-id="0">
                                                <input type="text" name="assemblages[0][name]" class="form-control" placeholder="Name" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][left_point_value]" class="form-control text-right" placeholder="Left Point Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][left_midpoint_value]" class="form-control text-right" placeholder="Left Midpoint Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][right_midpoint_value]" class="form-control text-right" placeholder="Right Midpoint Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][right_point_value]" class="form-control text-right" placeholder="Right Value" value="" required>

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                        </div>
                                        @endforelse
                                        @else
                                        @if ($variable->assemblages->count())
                                        <div class="form-group mb-2" data-type="assemblages">
                                            @forelse($variable->assemblages as $assemblage)
                                            <div class="input-group mb-2" data-id="{{ $assemblage->id }}">
                                                <input type="text" name="assemblages[{{ $assemblage->id }}][name]" class="form-control" placeholder="Name" value="{{ $assemblage->name }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblage->id }}][left_point_value]" class="form-control text-right" placeholder="Left Point Value" value="{{ $assemblage->left_point_value }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblage->id }}][left_midpoint_value]" class="form-control text-right" placeholder="Left Midpoint Value" value="{{ $assemblage->left_midpoint_value }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblage->id }}][right_midpoint_value]" class="form-control text-right" placeholder="Right Midpoint Value" value="{{ $assemblage->right_midpoint_value }}" required>
                                                <input type="number" step="any" min="0" name="assemblages[{{ $assemblage->id }}][right_point_value]" class="form-control text-right" placeholder="Right Value" value="{{ $assemblage->right_point_value }}" required>

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                            @empty
                                            <div class="form-group mb-2" data-type="assemblages">
                                                <div class="input-group mb-2" data-id="0">
                                                    <input type="text" name="assemblages[0][name]" class="form-control" placeholder="Name" value="" required>
                                                    <input type="number" step="any" min="0" name="assemblages[0][left_point_value]" class="form-control text-right" placeholder="Left Point Value" value="" required>
                                                    <input type="number" step="any" min="0" name="assemblages[0][left_midpoint_value]" class="form-control text-right" placeholder="Left Midpoint Value" value="" required>
                                                    <input type="number" step="any" min="0" name="assemblages[0][right_midpoint_value]" class="form-control text-right" placeholder="Right Midpoint Value" value="" required>
                                                    <input type="number" step="any" min="0" name="assemblages[0][right_point_value]" class="form-control text-right" placeholder="Right Value" value="" required>

                                                    <div class="input-group-append">
                                                        <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforelse
                                        </div>
                                        @else
                                        <div class="form-group mb-2" data-type="assemblages">
                                            <div class="input-group mb-2" data-id="0">
                                                <input type="text" name="assemblages[0][name]" class="form-control" placeholder="Name" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][left_point_value]" class="form-control text-right" placeholder="Left Point Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][left_midpoint_value]" class="form-control text-right" placeholder="Left Midpoint Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][right_midpoint_value]" class="form-control text-right" placeholder="Right Midpoint Value" value="" required>
                                                <input type="number" step="any" min="0" name="assemblages[0][right_point_value]" class="form-control text-right" placeholder="Right Value" value="" required>

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endif

                                        <div class="mt-2 text-center">
                                            <button type="button" class="list-add-button btn btn-sm btn-success pt-1 pb-1 ">
                                                <i class="material-icons">add</i>
                                                Add Assemblage
                                            </button>
                                        </div>

                                        @error('assemblages.*')
                                        <input type="text" class="form-control is-invalid" hidden>
                                        <div class="invalid-feedback">Something went wrong!</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                            <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                <i class="material-icons">save</i>
                                Save
                            </button>
                            <a class="btn btn-sm btn-secondary text-white mt-1 mb-1" href="{{ route('administrator.variables.index') }}" style="width: 80px;">
                                <i class="material-icons">reply</i>
                                Cancel
                            </a>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
    $(".list-add-button").on("click", function() {
        const list = $(this).parent().prev();
        const listType = list.data("type");
        const lastList = list.children().last();
        const currentListId = parseInt(lastList.data('id')) + 1;

        list.append(`
            <div class="input-group mb-2" data-id="${currentListId}">
                <input type="text" name="${listType}[${currentListId}][name]" class="form-control" placeholder="Name" value="" required>
                <input type="number" step="any" min="0" name="${listType}[${currentListId}][left_point_value]" class="form-control text-right" placeholder="Left Point Value" value="" required>
                <input type="number" step="any" min="0" name="${listType}[${currentListId}][left_midpoint_value]" class="form-control text-right" placeholder="Left Midpoint Value" value="" required>
                <input type="number" step="any" min="0" name="${listType}[${currentListId}][right_midpoint_value]" class="form-control text-right" placeholder="Right Midpoint Value" value="" required>
                <input type="number" step="any" min="0" name="${listType}[${currentListId}][right_point_value]" class="form-control text-right" placeholder="Right Value" value="" required>

                <div class="input-group-append">
                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                </div>
            </div>
        `);
    });

    $(document).on("click", ".list-destroy-button", function() {
        $(this).parent().parent().remove();
    });

    $("#type").on("change", function() {
        if ($(this).val() == "INPUT") {
            $("#column-name").prop("disabled", false);
        } else {
            $("#column-name").prop("disabled", true);
        }
    });
</script>
@endsection