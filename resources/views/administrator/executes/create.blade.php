@extends('administrator.layouts.template')

@section('css')
<link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" rel="stylesheet">

<style>
    .dataTables_length,
    .dataTables_filter,
    .dataTables_info,
    .dataTables_paginate {
        margin-left: 1.1rem !important;
        margin-right: 1.1rem !important;
    }

    .dataTables_length,
    .dataTables_info {
        text-align: left;
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
@endsection

@section('content')
@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Create Execute</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <form method="POST" action="{{ route('administrator.executes.store') }}" enctype="multipart/form-data">
                        @csrf

                        <li class="list-group-item p-4">
                            <div class="row mb-4">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-1">There are things that need to be considered before executing</strong>
                                    <ul class="small">
                                        <li>An example of an import format can be seen <a href=" https://docs.google.com/spreadsheets/d/1Nptw4MiSin_yaRAzDz6nxJEyNz9GyudGiDa7yeXCM_0" target="_blank">here</a>.</li>
                                        <li>The header names in the CSV file are {{ join(' and ', array_filter(array_merge(array(join(', ', array_slice($inputVariables, 0, -1))), array_slice($inputVariables, -1)), 'strlen')) }}.</li>
                                        <li>Make sure that there is only one output variable.</li>
                                        <li>Make sure there are no rules that are lost or missed.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <strong class="text-muted d-block mb-2">Recapitulation File</strong>
                                    <div class="form-group">
                                        <input type="file" name="file" class="form-control" required>

                                        @error("file")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <strong class="text-muted d-block mb-2">Multiplier of Results</strong>
                                    <div class="form-group">
                                        <input type="number" name="multiplier_result" class="form-control text-right" placeholder="Multiplier of results" value="{{ old('multiplier_result', 1) }}" required>

                                        @error("multiplier_result")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <strong class="text-muted d-block mb-2">Decimal Digits</strong>
                                    <div class="form-group">
                                        <input type="number" name="decimal_digit" class="form-control text-right" placeholder="Number of decimal digits" value="{{ old('decimal_digit', 2) }}" required>

                                        @error("decimal_digit")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                            <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 100px;">
                                <i class="material-icons">play_arrow</i>
                                Executes
                            </button>
                            <!-- <a class="btn btn-sm btn-secondary text-white mt-1 mb-1" href="{{ route('administrator.executes.index') }}" style="width: 80px;">
                                <i class="material-icons">reply</i>
                                Cancel
                            </a> -->
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>

    @if ($successData = session()->get("success_data"))
    <div class="row mt-1">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Success Data</h6>
                </div>

                <div class="card-body p-0 pt-3 pb-2 table-responsive">
                    <table id="table" class="table table-hover w-100">
                        <thead class="bg-light">
                            <tr class="text-center">
                                <th scope="col" class="border-0" style="width: 50px;">#</th>
                                <th scope="col" class="border-0">Description</th>
                                <th scope="col" class="border-0">Result</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($successData as $data)
                            <tr>
                                <td class="align-middle text-center">{{ (@$i += 1) }}</td>
                                <td class="align-middle">{{ $data["description"] }}</td>
                                <td class="align-middle text-right">{{ number_format($data["defuzzification"]["value"], old('decimal_digit', 2)) }}</td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if ($failedData = session()->get("failed_data"))
    <div class="row mt-1">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Failed Data</h6>
                </div>

                <div class="card-body p-0 pt-3 pb-2 table-responsive">
                    <table id="table" class="table table-hover w-100">
                        <thead class="bg-light">
                            <tr class="text-center">
                                <th scope="col" class="border-0" style="width: 50px;">#</th>
                                <th scope="col" class="border-0">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($failedData as $data)
                            <tr>
                                <td class="align-middle text-center">{{ (@$j += 1) }}</td>
                                <td class="align-middle">{{ $data["description"] }}</td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(".table").DataTable({
        responsive: true,
        language: {
            paginate: {
                previous: "‹",
                next: "›"
            }
        },
    })
</script>
@endsection