@extends('administrator.layouts.template')

@section('content')
@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Add User</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <form method="POST" action="{{ route('administrator.users.store') }}">
                        @csrf

                        <li class="list-group-item p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Name <small class="text-danger font-weight-bold">*</small></strong>
                                    <div class="form-group">
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required>

                                        @error("name")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Email <small class="text-danger font-weight-bold">*</small></strong>
                                    <div class="form-group">
                                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required>

                                        @error("email")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <strong class="text-muted d-block mb-2">Password <small class="text-danger font-weight-bold">*</small></strong>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" autocomplete="new-password" required>

                                        @error("password")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <strong class="text-muted d-block mb-2">Confirm Password <small class="text-danger font-weight-bold">*</small></strong>
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm" autocomplete="new-password" required>
                                    </div>
                                </div>
                            </div>

                            @if ($authUser->hasRole($settingRole["administrator"]))
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Role <small class="text-danger font-weight-bold">*</small></strong>
                                    <div class="form-group container-fluid row p-0 m-0">
                                        @foreach ($roles as $itemId => $item)
                                        <div class="col-md-3 col-sm-4 custom-control custom-checkbox mb-1">
                                            <input type="checkbox" id="role[{{ $itemId }}]" name="role[]" class="custom-control-input" value="{{ $itemId }}" {{ (!empty(old('role')) && in_array($itemId, old('role'))) || (empty(old('role')) && $itemId === 2) ? "checked" : "" }}>
                                            <label class="custom-control-label" for="role[{{ $itemId }}]">{{ $item }}</label>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group m-0">
                                        <input class="form-control @error('role') is-invalid @enderror" hidden>
                                        @error("role")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            @endif
                        </li>

                        <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                            <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                <i class="material-icons">save</i>
                                Save
                            </button>
                            <a class="btn btn-sm btn-secondary text-white mt-1 mb-1" href="{{ route('administrator.users.index') }}" style="width: 80px;">
                                <i class="material-icons">reply</i>
                                Cancel
                            </a>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection