@extends('administrator.layouts.template')

@section('css')
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
@endsection

@section('content')
@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Create Rule</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <form method="POST" action="{{ route('administrator.rules.store') }}">
                        @csrf

                        <li class="list-group-item p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Value</strong>
                                    <div class="form-group">
                                        {!! Form::select('value', $variableAssemblages, old('value'), [
                                        'class' => 'form-control' . ($errors->has('value') ? ' is-invalid' : '' ),
                                        'id' => 'value',
                                        'placeholder' => 'Select one...',
                                        'required' => 'required'
                                        ]) !!}

                                        @error("value")
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <strong class="text-muted d-block mb-2">Conditions</strong>
                                    <div class="form-group">
                                        <div class="form-group mb-2" data-type="conditions">
                                            @if (old('conditions') && is_array(old('conditions')))
                                            @forelse(old('conditions') as $conditionKey => $condition)
                                            <div class="input-group mb-2" data-id="{{ $conditionKey }}">
                                                <div class="input-group-prepend">
                                                    {!! Form::select('conditions['.$conditionKey.'][variable]', $variables, '', [
                                                    'class' => 'variable form-control' . ($errors->has('conditions.'.$conditionKey.'.variable') ? ' is-invalid' : '' ),
                                                    'placeholder' => 'Select one...',
                                                    'required' => 'required'
                                                    ]) !!}
                                                </div>
                                                {!! Form::select('conditions['.$conditionKey.'][assemblage]', [], '', [
                                                'class' => 'assemblage form-control' . ($errors->has('conditions.'.$conditionKey.'.assemblage') ? ' is-invalid' : '' ),
                                                'placeholder' => 'Select one...',
                                                'required' => 'required',
                                                'disabled' => ''
                                                ]) !!}

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                            @empty
                                            <div class="input-group mb-2" data-id="0">
                                                <div class="input-group-prepend">
                                                    {!! Form::select('conditions[0][variable]', $variables, '', [
                                                    'class' => 'variable form-control',
                                                    'placeholder' => 'Select one...',
                                                    'required' => 'required'
                                                    ]) !!}
                                                </div>
                                                {!! Form::select('conditions[0][assemblage]', [], '', [
                                                'class' => 'assemblage form-control',
                                                'placeholder' => 'Select one...',
                                                'required' => 'required',
                                                'disabled' => ''
                                                ]) !!}

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                            @endforelse
                                            @else
                                            <div class="input-group mb-2" data-id="0">
                                                <div class="input-group-prepend">
                                                    {!! Form::select('conditions[0][variable]', $variables, '', [
                                                    'class' => 'variable form-control',
                                                    'placeholder' => 'Select one...',
                                                    'required' => 'required'
                                                    ]) !!}
                                                </div>
                                                {!! Form::select('conditions[0][assemblage]', [], '', [
                                                'class' => 'assemblage form-control',
                                                'placeholder' => 'Select one...',
                                                'required' => 'required',
                                                'disabled' => ''
                                                ]) !!}

                                                <div class="input-group-append">
                                                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                                                </div>
                                            </div>
                                            @endif
                                        </div>

                                        <div class="mt-2 text-center">
                                            <button type="button" class="list-add-button btn btn-sm btn-success pt-1 pb-1 ">
                                                <i class="material-icons">add</i>
                                                Add Condition
                                            </button>
                                        </div>

                                        @error("conditions.*")
                                        <input type="text" class="form-control is-invalid" hidden>
                                        <div class="invalid-feedback">Something went wrong!</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item pt-3 pb-3 pl-4 pr-4 text-left">
                            <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                <i class="material-icons">save</i>
                                Save
                            </button>
                            <a class="btn btn-sm btn-secondary text-white mt-1 mb-1" href="{{ route('administrator.rules.index') }}" style="width: 80px;">
                                <i class="material-icons">reply</i>
                                Cancel
                            </a>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
    $(".list-add-button").on("click", function() {
        const list = $(this).parent().prev();
        const listType = list.data("type");
        const lastList = list.children().last();
        const currentListId = parseInt(lastList.data('id')) + 1;

        list.append(`
            <div class="input-group mb-2" data-id="${currentListId}">
                <div class="input-group-prepend">
                    <select name="${listType}[${currentListId}][variable]" class="variable form-control" required>
                        <option value="">Select one...</option>
                        @forelse ($variables as $variableId => $variableName)
                        <option value="{{ $variableId }}">{{ $variableName }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <select name="${listType}[${currentListId}][assemblage]" class="assemblage form-control" required="required" disabled="">
                    <option selected="selected" value="">Select one...</option>
                </select>

                <div class="input-group-append">
                    <button type="button" class="list-destroy-button btn btn-sm btn-danger">Destroy</button>
                </div>
            </div>
        `);
    });

    $(document).on("click", ".list-destroy-button", function() {
        $(this).parent().parent().remove();
    });

    $(document).on("change", ".variable", function() {
        const id = $(this).parent().parent().data('id');
        const variableId = $(this).val();
        const assemblage = $('[name="conditions[' + id + '][assemblage]"]');

        if (variableId) {
            $.ajax({
                type: "GET",
                url: `{{ route("api.variables.index") }}/${variableId}`,
                beforeSend: function() {
                    assemblage.prop("disabled", true);
                    assemblage.find("option").remove();
                    assemblage.append(new Option("Select one...", ""));
                },
                success: function(res) {
                    if (res.meta.code === 200) {
                        res.data.assemblages.map(data => {
                            assemblage.append(new Option(data.name, data.id));
                        });

                        assemblage.prop("disabled", false);
                    } else {
                        alert(res.meta.message);
                    }
                },
                error: function(xhr, status, error) {
                    alert("Try again.");
                }
            });
        } else {
            assemblage.prop("disabled", true);
            assemblage.find("option").remove();
            assemblage.append(new Option("Select one...", ""));
        }
    });

    <?php
    if (old('conditions') && is_array(old('conditions'))) {
    ?>
        handleSelectAssemblageVariable = (id, variableId, assemblageId = null) => {
            const assemblage = $('[name="conditions[' + id + '][assemblage]"]');

            $.ajax({
                type: "GET",
                url: `{{ route("api.variables.index") }}/${variableId}`,
                beforeSend: function() {
                    assemblage.prop("disabled", true);
                    assemblage.find("option").remove();
                    assemblage.append(new Option("Select one...", ""));
                },
                success: function(res) {
                    if (res.meta.code === 200) {
                        res.data.assemblages.map(data => {
                            assemblage.append(new Option(data.name, data.id));
                        });

                        assemblage.prop("disabled", false);

                        if (assemblageId) {
                            assemblage.val(assemblageId);
                            if (assemblage === null) {
                                assemblage.val("").change();
                            }
                        }
                    }
                }
            });
        }

        <?php
        foreach (old('conditions') as $conditionKey => $condition) {
            if (is_array($condition) && array_key_exists("variable", $condition)) {
        ?>
                var variable = $('[name="conditions[<?= $conditionKey ?>][variable]"]');
                variable.val(<?= $condition["variable"] ?>);
                if (variable === null) {
                    variable.val("").change();
                } else {
                    handleSelectAssemblageVariable(<?= $conditionKey ?>, <?= $condition["variable"] ?>, <?= array_key_exists("assemblage", $condition) ? $condition["assemblage"] : '' ?>)
                }
    <?php
            }
        }
    }
    ?>
</script>
@endsection