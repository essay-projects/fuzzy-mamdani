<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPointValueToVariableAssemblagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variable_assemblages', function (Blueprint $table) {
            $table->float('left_point_value', 10, 2)->after('name');
            $table->float('left_midpoint_value', 10, 2)->after('left_point_value');
            $table->float('right_midpoint_value', 10, 2)->after('left_midpoint_value');
            $table->float('right_point_value', 10, 2)->after('right_midpoint_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variable_assemblages', function (Blueprint $table) {
            $table->dropColumn('left_point_value');
            $table->dropColumn('left_midpoint_value');
            $table->dropColumn('right_midpoint_value');
            $table->dropColumn('right_point_value');
        });
    }
}
