<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariableAssemblagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variable_assemblages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('variable_id')->constrained('variables')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->integer('lowest_value');
            $table->integer('highest_value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variable_assemblages');
    }
}
