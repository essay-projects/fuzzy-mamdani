<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropLowestAndHighestValueFromVariableAssemblagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variable_assemblages', function (Blueprint $table) {
            $table->dropColumn('lowest_value');
            $table->dropColumn('highest_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variable_assemblages', function (Blueprint $table) {
            $table->integer('lowest_value')->after('right_point_value');
            $table->integer('highest_value')->after('lowest_value');
        });
    }
}
