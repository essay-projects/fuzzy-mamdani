<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            // Master Data
            PermissionSeeder::class,

            // User
            PersonSeeder::class,
            UserSeeder::class

            // Relation
        ]);
    }
}
