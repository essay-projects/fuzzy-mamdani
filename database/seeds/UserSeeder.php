<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $superadminUser = User::updateOrCreate(
            [
                "person_id" => 1
            ],
            [
                "username" => "superadmin",
                "email" => "superadmin@sintastech.com",
                "password" => bcrypt("yogyakarta"),
                "is_active" => 1
            ]
        );
        $role = Role::updateOrCreate(["id" => 1], ["name" => "Super Admin"]);
        $permissions = Permission::pluck("id", "id")->all();
        $role->syncPermissions($permissions);
        $superadminUser->assignRole([$role->id]);

        $adminUser = User::updateOrCreate(
            [
                "person_id" => 2
            ],
            [
                "username" => "admin",
                "email" => "admin@sintastech.com",
                "password" => bcrypt("indonesia"),
                "is_active" => 1
            ]
        );
        $role = Role::updateOrCreate(["id" => 2], ["name" => "Admin"]);
        $adminUser->assignRole([$role->id]);
    }
}
